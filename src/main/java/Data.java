import com.google.common.collect.ImmutableList;
import java.io.*;
import java.util.List;

public class Data {
    private List<String> listNames;
    private List<String> listSurnames;
    private List<String> listDomains;
    private final String FilePathNames = "/src/main/resources/names.txt";
    private final String FilePathSurnames = "/src/main/resources/surnames.txt";
    private final String FilePathDomains = "/src/main/resources/domain.txt";

    Data(List<String> listNames, List<String> listSurnames, List<String> listDomains) {
        this.listNames = listNames;
        this.listSurnames = listSurnames;
        this.listDomains = listDomains;
    }

    public List<String> getListNames() throws IOException {
        if (listNames.size() == 0)
            readFile(listNames, FilePathNames);

        return ImmutableList.copyOf(listNames);
    }

    public List<String> getListSurnames() throws IOException {
        if (listSurnames.size() == 0)
            readFile(listSurnames, FilePathSurnames);

        return ImmutableList.copyOf(listSurnames);
    }

    public List<String> getListDomain() throws IOException {
        if (listDomains.size() == 0) {
            readFile(listDomains, FilePathDomains);
        }
        return ImmutableList.copyOf(listDomains);
    }

    private void readFile(List<String> list, String filePath) throws IOException {
        String workingDirectory = System.getProperty("user.dir");
        String absoluteFilePath = workingDirectory + File.separator + filePath;
        FileReader fileReader = new FileReader(absoluteFilePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        list.clear();

        try {
            String textLine = bufferedReader.readLine();
            do {
                list.add(textLine);

                textLine = bufferedReader.readLine();
            } while (textLine != null);
        } finally {
            bufferedReader.close();
        }
    }
}
