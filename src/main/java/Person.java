public class Person {
    protected String name;
    protected String surname;
    protected String email;

    public Person(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    private boolean checkName(String name) {
        return name.matches("^[a-zA-Z]+$");
    }

    private boolean checkSurname(String surname) {
        return surname.matches("^[a-zA-Z]+$");
    }

    private boolean checkEmail(String email) {
        return email.matches("^[a-zA-Z]+@[a-zA-Z]+.[a-zA-Z]+$");
    }

    public String toString() {

        if (checkName(name) && checkSurname(surname) && checkEmail(email)) {
            return "[Name: " + name + " Surname: " + surname + " email: " + email + "]\n";
        }
        throw new IllegalArgumentException("Wrong string\n");
    }
}