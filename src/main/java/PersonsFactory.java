import com.google.common.collect.ImmutableList;
import java.util.*;
import java.io.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PersonsFactory {
    private Data data;
    private List<Person> people;
    private int numberOfPeople;
    private List<String> listNames;
    private List<String> listSurnames;
    private List<String> listDomains;

    PersonsFactory(List<String> names, List<String> surnames, List<String> domains, int n) {
        listNames = names;
        listSurnames = surnames;
        listDomains = domains;
        people = new ArrayList<Person>(numberOfPeople);
        data = new Data(listNames, listSurnames, listDomains);
        numberOfPeople = n;
    }

    public void RandomPersons() throws IOException {

        IntStream.range(0, numberOfPeople).forEach(i -> {
            try {
                String name = RandomElement(data.getListNames());
                String surname = RandomElement(data.getListSurnames());
                String email = name.toLowerCase() + surname.toLowerCase() + "@" + RandomElement(data.getListDomain());
                Person person = new Person(name, surname, email);
                people.add(person);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private String RandomElement(List<String> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }

    public void Sort() {
        Comparator<Person> byNames = Comparator.comparing(person -> person.name);
        Comparator<Person> bySurnames = Comparator.comparing(person -> person.surname);
        Comparator<Person> byEmail = Comparator.comparing(person -> person.email);

        people = people.stream().sorted(byNames.thenComparing(bySurnames).thenComparing(byEmail)).collect(Collectors.toList());
    }

    public String toString() {

        return ImmutableList.copyOf(people).toString();

    }


}
